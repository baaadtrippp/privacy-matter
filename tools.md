### Secure storage and transfer

[brig - decentralized & secure synchronization](https://brig.readthedocs.io/en/master/)

[Joplin is an open source note-taking app](https://joplinapp.org/)

[CROC - private file transfer](https://github.com/schollz/croc)

[Cryptee - a private place to write documents, store photos and more](https://crypt.ee)

[PrivateBin](https://privatebin.net/)

[Jami - GNU universal communication platform](https://jami.net/)


### Intruder detection

[CrowdSec is a free, modern & collaborative behavior detection engine](https://github.com/crowdsecurity/crowdsec)

[MVT - Mobile Verification Toolkit](https://github.com/mvt-project/mvt)


### Stenography and Metadata removal

[ImSter - Image Steganographer](https://github.com/armytricks/ImSter)

[mat2 - metadata removal tool](https://0xacab.org/jvoisin/mat2)

[ExifCleaner](https://exifcleaner.com/)

[Image "Cloaking" for Personal Privacy](http://sandlab.cs.uchicago.edu/fawkes/)


### Remote desktop

[RustDesk](https://github.com/rustdesk/rustdesk)

[UltraVNC remote access tools](https://www.uvnc.com/)


### Browsers and Add-Ons

[Disable WebRTC firefox](https://addons.mozilla.org/ru/firefox/addon/happy-bonobo-disable-webrtc/)

[Disable WebRTC Control chrome](https://chrome.google.com/webstore/detail/webrtc-control/fjkmabmdepjfammlpliljpnbhleegehm)


### Misc.

[vedbex, fakeproof and more](https://www.vedbex.com/fakeproof)

[CamGAG - fake video xxx models](https://camgag.com/camgag/index.php)

[XENOTPRO - fakeproof and more](https://xenot.pro/)




