# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

### Linux tuts ###

* [How To Set up OpenVPN Server In 5 Minutes on Ubuntu Linux](https://www.cyberciti.biz/faq/howto-setup-openvpn-server-on-ubuntu-linux-14-04-or-16-04-lts/)
* [Setting up an OpenVPN server from scratch on Ubuntu 20.04](https://notthebe.ee/Creating-your-own-OpenVPN-server.html)
* [OpenVPN road warrior installer for Ubuntu, Debian, CentOS and Fedora](https://github.com/Nyr/openvpn-install)
* [How to Set Up SSH Keys on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04)
* [How To Configure SSH Key-Based Authentication on a Linux Server](https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server)

### TOR ###

* [Tor Protocol Specification](https://github.com/torproject/torspec/blob/master/tor-spec.txt)
* [DoHoT: making practical use of DNS over HTTPS over Tor](https://github.com/alecmuffett/dohot)
* [txtorcon - control tor from twisted](https://txtorcon.readthedocs.io/en/latest/introduction.html)
* [Introduction to Tor Browser](https://www.oilandfish.com/posts/introduction-to-tor-browser.html)

### Ethereum and ENS ###

* [Ethereum name service - name / addresses search](https://app.ens.domains/)
* [ENS Docs](https://docs.ens.domains/)
* [Decentralized naming for wallets, websites, & more](https://ens.domains/)
* [Ethereum Name Service github page](https://github.com/ensdomains)
* [The Ultimate ENS and ĐApp Tutorial](https://www.toptal.com/dapp/ethereum-name-service-dapp-tutorial)
* [Web3.py is a Python library for interacting with Ethereum](https://web3py.readthedocs.io/en/stable/)
* [ContentHash for Python](https://github.com/filips123/ContentHashPy)
* [This EIP introduces the new contenthash field for ENS resolvers](https://github.com/ethereum/EIPs/blob/master/EIPS/eip-1577.md)
* [Official Go implementation of the Ethereum protocol](https://geth.ethereum.org/)
* [sTorGate](https://github.com/ricott1/storgate)
* [Everything You Can Do With Your ENS Name Right Now](https://medium.com/the-ethereum-name-service/everything-you-can-do-with-your-ens-name-right-now-9a66763e970a)
* [Step-by-Step Guide to Registering a .ETH Name](https://medium.com/the-ethereum-name-service/step-by-step-guide-to-registering-a-eth-name-on-the-new-ens-registrar-c07d3ab9d6a6)
* [How Secure Is Using ENS for Tor .Onion Addresses?](https://medium.com/the-ethereum-name-service/how-secure-is-using-ens-for-tor-onion-addresses-85b22f44b6e0)
* [List of ENS Names that Resolve to Tor .Onion Websites](https://medium.com/the-ethereum-name-service/list-of-ens-names-that-resolve-to-tor-onion-websites-99140a4c674f)
* [Infura's world-class infrastructure will ensure your decentralized application scales to meet your user demand](https://infura.io/)
* [MetaMask Browser Extension](https://github.com/ricott1/metamask-extension)
* [Add ENS Domains to Sites on IPFS](https://fleek.co/ens-domains/)
* [ENS Contracts](https://github.com/ensdomains/ens/blob/master/contracts/ENS.sol)
* [ENS Resolver](https://github.com/ensdomains/resolvers/blob/master/contracts/Resolver.sol)

### Monero ###

* [PiNode-XMR](https://github.com/monero-ecosystem/PiNode-XMR)
* [KYC NOT exchanges](https://kycnot.me/)
* [Monero Integrations](Monero Integrations)
* [Pay with Monero](https://cryptwerk.com/pay-with/xmr/)

### Haven ###

* [Haven](https://havenprotocol.org/)

### General Crypto ###

* [3commans  - Smart Trading terminal and auto trading bots](https://3commas.io/)
* [Trading Bots and More](https://3c.exchange/)
* [TOR Browser distros](https://dist.torproject.org/torbrowser/)
* [TORCC manual](https://helpmanual.io/man5/torrc/)

### SBC fun ###

* [How to set up the Raspberry Pi Zero for travel](https://opensource.com/article/20/3/raspberry-pi-zero)
* [Adding a display to a travel-ready Raspberry Pi Zero](https://opensource.com/article/20/3/pi-zero-display)
* [Set up a Tor proxy with Raspberry Pi to control internet traffic](https://opensource.com/article/20/4/tor-proxy-raspberry-pi)
* [Raspberry Pi - Auto WiFi Hotspot Switch - Direct Connection](https://www.raspberryconnect.com/projects/65-raspberrypi-hotspot-accesspoints/158-raspberry-pi-auto-wifi-hotspot-switch-direct-connection)
* [RPI zero W as both Wifi client and access point](https://raspberrypi.stackexchange.com/questions/63841/rpi-zero-w-as-both-wifi-client-and-access-point)
* [Fuzix Unix-like operating system ported to Raspberry Pi Pico and ESP8266](https://www.cnx-software.com/2021/02/23/fuzix-unix-like-operating-system-ported-to-raspberry-pi-pico-and-esp8266/)
* [Make a Raspberry Pi USB TOR-stick](https://medium.com/@jcolond/make-a-raspberry-pi-usb-tor-stick-2d494e7f81ea)
* [Setting up Pi Zero OTG - The quick way (No USB keyboard, mouse, HDMI monitor needed)](https://gist.github.com/gbaman/975e2db164b3ca2b51ae11e45e8fd40a)

### Publications ###

* [N-O-D-E](https://n-o-d-e.net/)
* [P L U J A Censorship reveals fear.](https://pluja.dev/)

### General ###

* [addy 2 is a powerful, enterprise-ready, open source web server with automatic HTTPS written in Go](https://caddyserver.com/)
* [Yotter youtube and twitter with privacy](https://github.com/ytorg/Yotter)
