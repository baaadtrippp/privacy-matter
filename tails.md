## Tails OS

[Tails](https://tails.boum.org/)

[How to install and use VeraCrypt on Tails](https://sunknudsen.com/privacy-guides/how-to-install-and-use-veracrypt-on-tails#how-to-install-and-use-veracrypt-on-tails)

[Using VeraCrypt encrypted volumes](https://tails.boum.org/doc/encryption_and_privacy/veracrypt/index.en.html)