## Security and Privacy articles


[SECURITY IN A BOX – DIGITAL-SECURITY TOOLS & TACTICS](https://securityinabox.org/en/)

[SURVEILLANCE SELF-DEFENSE](https://ssd.eff.org/)

[Как быстро и надёжно удалить файлы, зачистить HDD/SSD перед обыском](https://telegra.ph/Kak-bystro-i-nadyozhno-udalit-fajly-zachistit-HDDSSD-pered-obyskom-07-01)

[The Hitchhiker’s Guide to Online Anonymity on TOR](http://thgtoa7imksbg7rit4grgijl2ef6kc7b56bp56pmtta4g354lydlzkqd.onion/guide.html)



